module "resource_g" {
  source = "../azure_resource_group"
}

resource "azurerm_postgresql_server" "example" {
  name                = "ajith-postgresql-server"
  location            = module.resource_g.rg_location
  resource_group_name = module.resource_g.rg_name

  sku_name = "B_Gen5_2"

  storage_mb                   = 5120
  backup_retention_days        = 7
  geo_redundant_backup_enabled = false
  auto_grow_enabled            = false

  administrator_login          = "psqladmin"
  administrator_login_password = "H@Sh1CoR3!"
  version                      = "9.5"
  ssl_enforcement_enabled      = false
}

resource "azurerm_postgresql_database" "example" {
  name                = "ajith-exampledb"
  resource_group_name = module.resource_g.rg_name
  server_name         = azurerm_postgresql_server.example.name
  charset             = "UTF8"
  collation           = "English_United States.1252"
}