resource "azurerm_resource_group" "rg" {
  name     = "rg-main-stingray"
  location = var.resource_group_location
}

resource "azurerm_kubernetes_cluster" "Ajith_Cluster" {
  location = azurerm_resource_group.rg.location
  name     = "Ajith_Cluster"
  resource_group_name = azurerm_resource_group.rg.name
  dns_prefix = "Ajith-Cluster-dns"

  kubernetes_version = "1.23.12"
  

  default_node_pool {
    node_count      = 2
    name            = "default"
    vm_size         = "Standard_D2_v2"
    os_disk_size_gb = 30
  }

  service_principal {
    client_id     = var.az_client_id
    client_secret = var.az_client_secret
  }

  role_based_access_control_enabled = true

  tags = {
    test = 1.0
  }

}
