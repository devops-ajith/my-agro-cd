output "rg_name"{
    value = azurerm_resource_group.rg.name
    description = "name of rg"
}

output "rg_location" {
  value = azurerm_resource_group.rg.location
  description = "location of rg"
}